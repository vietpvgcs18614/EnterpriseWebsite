import { environment } from 'src/environments/environment';

export class BaseURL {
    public static DIENHAU = environment.API_DIEN_HAU + '/UserInformation';
    public static BANTHAN = environment.API_BAN_THAN + '/UserInformation';
}

export class CommandURL {

  // ADMIN
    // public static ACCOUNTS = BaseURL.DIENHAU;
    public static ACCOUNTS = BaseURL.BANTHAN;
}
