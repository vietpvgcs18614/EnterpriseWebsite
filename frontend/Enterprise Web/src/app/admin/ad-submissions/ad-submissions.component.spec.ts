import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdSubmissionsComponent } from './ad-submissions.component';

describe('AdSubmissionsComponent', () => {
  let component: AdSubmissionsComponent;
  let fixture: ComponentFixture<AdSubmissionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdSubmissionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdSubmissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
