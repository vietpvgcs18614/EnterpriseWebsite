import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';

@Component({
  selector: 'app-ad-submissions',
  templateUrl: './ad-submissions.component.html',
  styleUrls: ['./ad-submissions.component.css']
})
export class AdSubmissionsComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  yearBefore = new Date().getFullYear()+1;
  yearList : string[] = [];

  falList = ["IT", "Marketing", "Business", "Graphic Design"];

  ngOnInit(): void {
    this.makeYearList();
  }

  makeYearList() {
    for (let i = 0; i < 7; i++) {
        this.yearList.push(this.yearBefore.toString());
        this.yearBefore--;
    }
  }

  chooseFile() {
    $("#fileInput").click();
 }
  createModal(create: any){
    this.modalService.open(create, { centered: true, size: "lg" });
  }

  editModal(edit: any){
    this.modalService.open(edit, { centered: true, size: "lg" });
  }

  closeModal(){
    this.modalService.dismissAll();
  }
}
