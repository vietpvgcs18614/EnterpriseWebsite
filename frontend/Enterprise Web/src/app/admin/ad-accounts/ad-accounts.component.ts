import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { ACCOUNTs } from './ad-accounts';
import { AdAccountsService } from './ad-accounts.service';

@Component({
  selector: 'app-ad-accounts',
  templateUrl: './ad-accounts.component.html',
  styleUrls: ['./ad-accounts.component.css']
})
export class AdAccountsComponent implements OnInit {

  constructor(  private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private toaster: ToastrService,
    private accountsService: AdAccountsService ) { }

roleList = ["Admin", "Marketing Manager", "Marketing Coordinator", "Student", "Guest"];
falList = ["IT", "Marketing", "Business", "Graphic Design"];

nam = 2020;
pageNum = 1;
pageSize = 10;
searchKey = '';
totalRecords: number;

accountList = [];

updated = false;
submitted = false;

btnSubmitName = 'Create';

id: any;

accountForm: FormGroup;

ngOnInit(): void {

  this.accountForm = this.formBuilder.group({
    codeUser : ['', Validators.required],
    name : ['', Validators.required],
    email : ['', Validators.required],
    dob : ['', Validators.required],
    address : ['', Validators.required],
    phoneNumber :  ['', Validators.required],
    nam :  ['', Validators.required],
  })

  this.getCategoryList();
}

  onReset(){
    this.updated = false;
    this.submitted = false;
  }

  viewModal(item: ACCOUNTs, view: any) {
    if (item){
      this.id = item.id;
      this.accountForm.patchValue(item);
    } else {
      this.onReset();
      this.updated = false;
      this.btnSubmitName = 'Create';
    }
    this.modalService.open( view, { size: 'lg', centered: true });
  }

  editModal(item: ACCOUNTs, edit: any) {
    if (item){
      this.id = item.id;
      this.accountForm.patchValue(item);
      this.submitted = false
      console.log('1');
      this.updated = true;
      this.btnSubmitName = 'Update'
    } else {
      this.updated = false;
      this.submitted = true;
      this.btnSubmitName = 'Createzalo';
      this.onReset();
    }
    this.modalService.open( edit, { size: 'lg', centered: true });
  }

  onSubmit(value: ACCOUNTs){

    this.submitted = true;

    if (this.accountForm.invalid){
      return;
    }

    if (this.updated){
      this.accountsService.updateCategory(this.id, value).subscribe( data => {
        if (data) {
          this.toaster.success('Thành Công', 'Cập nhật thông tin người dùng thành công');
          this.onReset();
          this.modalService.dismissAll();
          this.getCategoryList();
        } else {
          this.toaster.warning('Thất Bại', 'Cập nhật thông tin người dùng không thành công');
          this.onReset();
          this.modalService.dismissAll();
          this.getCategoryList();
        }
      }, (err) => {
          this.toaster.error('Thất Bại','Cập nhật thông tin đơn vị thất bại');
          this.submitted = false;
        })
    } else {
      this.accountsService.createNewCategory(value).subscribe( data => {
        console.log(data)
        if (data) {
          this.toaster.success('Thành Công', 'Tạo mới người dùng thành công');
          this.onReset();
          this.modalService.dismissAll();
          this.getCategoryList();
        }else {
          this.toaster.warning('Thất Bại', 'Tạo mới người dùng thất bại');
          this.onReset();
          this.modalService.dismissAll();
          this.getCategoryList();
        }
      },  (err) => {
          this.toaster.error('Thất Bại','Tạo mới thông tin người dùng thất bại');
          this.submitted = false;
        }
      )
    }
  }

  getCategoryList(){
    this.accountsService.getCategoryList(this.pageNum, this.pageSize, this.nam, this.searchKey).subscribe( data => {
      if ( data ){
        this.accountList = data.content;
        this.totalRecords = data.totalElements;
      }
    })
    console.log(this.accountList);
  }

  createModal(create: any) {
    this.modalService.open(create, { centered: true, size: 'md' });
  }



  deleteCategory(item: ACCOUNTs){
    const textConfirm = 'Dữ liệu bị xóa sẽ không thể khôi phục.';
    Swal.fire({
      title: '<span style="color: #2d8dc7; font-size: 1.5rem;"> Do you want to delete user <span style="color: #f1556c;">'
          + item.name + '</span> ?</span>',
      html: textConfirm,
      imageHeight: 150,
      imageWidth: 320,
      imageClass: 'img-responsive',
      animation: false,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '',
      cancelButtonText: 'Bỏ qua',
      confirmButtonText: 'Đồng ý'
    }).then((result) => {
      if (result.value) {
        this.accountsService.deleteCategory(item.id).subscribe(data => {
          if (data) {
            this.toaster.success('Thành công', 'Xóa thông tin người dùng thành công');
            this.getCategoryList();
            this.onReset();
            this.modalService.dismissAll();
          }
        });
      } (err) => {
          this.toaster.error('Thất bại', 'Xóa thông tin người dùng thất bại');
      }
    })
  }

  infoModal(info: any) {
    this.modalService.open(info, { centered: true, size: 'lg' });
  }

  closeModal(){
    this.modalService.dismissAll();
  }

}
