import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ad-faculties',
  templateUrl: './ad-faculties.component.html',
  styleUrls: ['./ad-faculties.component.css']
})
export class AdFacultiesComponent implements OnInit {

  constructor() { }

  layoutStatus: any = 2;

  ngOnInit(): void {

  }

  createLayout(){
    this.layoutStatus = 1;
  }

  bmLayout(){
    this.layoutStatus = 2;
  }

  itLayout(){
    this.layoutStatus = 3;
  }

  gdLayout(){
    this.layoutStatus = 4;
  }

  emLayout(){
    this.layoutStatus = 5;
  }

  marLayout(){
    this.layoutStatus = 6;
  }

}
