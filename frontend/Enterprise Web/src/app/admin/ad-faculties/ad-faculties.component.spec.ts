import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdFacultiesComponent } from './ad-faculties.component';

describe('AdFacultiesComponent', () => {
  let component: AdFacultiesComponent;
  let fixture: ComponentFixture<AdFacultiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdFacultiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdFacultiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
