import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AdAccountsComponent } from './admin/ad-accounts/ad-accounts.component';
import { AdFacultiesComponent } from './admin/ad-faculties/ad-faculties.component';
import { MdfLayoutComponent } from './mdf-layout/mdf-layout.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdSubmissionsComponent } from './admin/ad-submissions/ad-submissions.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { StdDashboardComponent } from './student/std-dashboard/std-dashboard.component';
import { StdChatComponent } from './student/std-chat/std-chat.component';
import { StdContributionComponent } from './student/std-contribution/std-contribution.component';
import { NotifyComponent } from './notify/notify.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ToastrModule } from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    AdAccountsComponent,
    AdFacultiesComponent,
    MdfLayoutComponent,
    AdSubmissionsComponent,
    StdDashboardComponent,
    StdChatComponent,
    StdContributionComponent,
    NotifyComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule,
    NgSelectModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates: false
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
