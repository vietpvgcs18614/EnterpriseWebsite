import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-std-contribution',
  templateUrl: './std-contribution.component.html',
  styleUrls: ['./std-contribution.component.css']
})
export class StdContributionComponent implements OnInit {

  constructor() { }

  layoutStatus = 7;

  ngOnInit(): void {
  }

  dfLayout(){
    this.layoutStatus = 1;
  }

  dbLayout(){
    this.layoutStatus = 2;
  }

  dgtLayout(){
    this.layoutStatus = 3;
  }

  nsLayout(){
    this.layoutStatus = 4;
  }

  ipmLayout(){
    this.layoutStatus = 5;
  }

  sbmLayout(){
    this.layoutStatus = 6;
  }

  sSbmLayout(){
    this.layoutStatus = 7;
  }

}
