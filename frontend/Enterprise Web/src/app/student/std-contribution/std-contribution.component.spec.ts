import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StdContributionComponent } from './std-contribution.component';

describe('StdContributionComponent', () => {
  let component: StdContributionComponent;
  let fixture: ComponentFixture<StdContributionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StdContributionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StdContributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
