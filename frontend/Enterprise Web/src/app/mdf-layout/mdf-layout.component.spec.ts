import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MdfLayoutComponent } from './mdf-layout.component';

describe('MdfLayoutComponent', () => {
  let component: MdfLayoutComponent;
  let fixture: ComponentFixture<MdfLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MdfLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MdfLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
