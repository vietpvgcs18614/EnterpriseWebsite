import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';


@Component({
  selector: 'app-mdf-layout',
  templateUrl: './mdf-layout.component.html',
  styleUrls: ['./mdf-layout.component.css']
})
export class MdfLayoutComponent implements OnInit {

  constructor( private router: Router,
               private modalServive: NgbModal)
               { }

  ngOnInit(): void {
    $("#menu-toggle1").click(function(e) {
      e.preventDefault();
      $("#sidebar").toggleClass("active");
      $("#menu").toggleClass("active");
      $("#content").toggleClass("active");
      $("#header").toggleClass("active");
      $("#footer").toggleClass("active");
    });

    $("#menu-toggle2").click(function(e) {
      e.preventDefault();
      $("#sidebar").toggleClass("active");
      $("#menu").toggleClass("active");
      $("#content").toggleClass("active");
      $("#header").toggleClass("active");
      $("#footer").toggleClass("active");
    });
  }

  profileModal(profile: any){
      this.modalServive.open( profile, { centered: true, size: 'lg' });
  }

  repassModal(repass: any){
    this.modalServive.open( repass, { centered: true, size: 'md' });
}

  logoutModal(logout: any){
    this.modalServive.open( logout, { centered: true, size: 'md' });
}

  notify(){
    this.router.navigate(['/notify']);
  }

  mngAccount(){
    this.router.navigate(['admin/account']);
  }

  mngFaculty(){
    this.router.navigate(['/admin/faculty']);
  }

  mngSubmit(){
    this.router.navigate(['/admin/submission']);
  }

  stdDash(){
    this.router.navigate(['admin/account']);
  }

  stdChat(){
    this.router.navigate(['/admin/faculty']);
  }

  stdContribution(){
    this.router.navigate(['/student/contribution']);
  }

  closeModal(){
    this.modalServive.dismissAll();
  }
}
