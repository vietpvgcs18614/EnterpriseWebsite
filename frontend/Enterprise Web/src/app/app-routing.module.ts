import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdAccountsComponent } from './admin/ad-accounts/ad-accounts.component';
import { AdFacultiesComponent } from './admin/ad-faculties/ad-faculties.component';
import { AdSubmissionsComponent } from './admin/ad-submissions/ad-submissions.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MdfLayoutComponent } from './mdf-layout/mdf-layout.component';
import { NotifyComponent } from './notify/notify.component';
import { StdChatComponent } from './student/std-chat/std-chat.component';
import { StdContributionComponent } from './student/std-contribution/std-contribution.component';
import { StdDashboardComponent } from './student/std-dashboard/std-dashboard.component';

const routes: Routes = [
    { path:'', component: LoginComponent, pathMatch:'full' },
    { path:'home', component: HomeComponent },
    { path:'mdflayout', component: MdfLayoutComponent },

    // Main
    {
      path: 'notify',
      component: MdfLayoutComponent,
      children: [
        { path: '', component: NotifyComponent }
      ]
    },

    // Admin
    {
      path: 'admin/account',
      component: MdfLayoutComponent,
      children: [
        { path: '', component: AdAccountsComponent }
      ]
    },

    {
      path: 'admin/submission',
      component: MdfLayoutComponent,
      children: [
        { path: '', component: AdSubmissionsComponent }
      ]
    },

    {
      path: 'admin/faculty',
      component: MdfLayoutComponent,
      children: [
        { path: '', component: AdFacultiesComponent }
      ]
    },

    // Student
    {
      path: 'student/dashboard',
      component: MdfLayoutComponent,
      children: [
        { path: '', component: StdDashboardComponent }
      ]
    },

    {
      path: 'student/chat',
      component: MdfLayoutComponent,
      children: [
        { path: '', component: StdChatComponent }
      ]
    },

    {
      path: 'student/contribution',
      component: MdfLayoutComponent,
      children: [
        { path: '', component: StdContributionComponent }
      ]
    },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
